import Button from "@suid/material/Button"
import Input from "@suid/material/Input"
import { invoke } from '@tauri-apps/api/tauri'

export default function Index() {
  const [imgSrc, setImgSrc] = createSignal('')
  const [file, setFile] = createSignal() // uploaded file path
  const handleClick = async () => {
    const res = await invoke('greet', {path: file()})
    console.log(res)
  }

  return (
    <div>
      <label for="contained-button-file">
        <Input
          accept="image/*"
          style={{ display: "none" }}
          id="contained-button-file"
          multiple
          type="file"
          onChange={e => setFile(e.currentTarget.value)}
        />
        <Button variant="contained" component="span">
          上传
        </Button>
      </label>
      <div class="mt-2">
        <Button variant="outlined" onClick={handleClick}>识别</Button>
        <img src={imgSrc()} alt="" />
      </div>
    </div>
  )
}
