use pyo3::{prelude::*};

// pub fn foo() -> Result<String, ()> {
//     let py_app = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/pycode/foo.py"));
//     //let mut res: String = String::from("");
//     let from_python = Python::with_gil(|py| -> PyResult<Py<PyAny>> {
//         let app: Py<PyAny> = PyModule::from_code(py, py_app, "", "")?
//             .getattr("run")?
//             .into();
//         //let kwargs = [("name", "foo")].into_py_dict(py);
//         //res = app.call(py, (), Some(kwargs))?.extract(py).unwrap();
//         app.call1(py, ())
//         //Ok(res)
//     });
//     Ok(from_python.unwrap().to_string())
// }

pub fn detect_img(path: String) -> Result<String, ()> {
    let py_app = include_str!(concat!(env!("CARGO_MANIFEST_DIR"), "/pycode/detect.py"));
    let from_python = Python::with_gil(|py| -> PyResult<Py<PyAny>> {
        let app: Py<PyAny> = PyModule::from_code(py, py_app, "", "")?
            .getattr("rust_call")?
            .into();
        app.call1(py, (path,))
    });
    Ok(from_python.unwrap().to_string())
}
